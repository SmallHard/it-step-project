"use strict";

const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');
const browserSync = require('browser-sync').create();
const plumber = require('gulp-plumber');
const compileHandlebars = require('gulp-compile-handlebars');
const trim = require('gulp-trim');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');

gulp.task("gulp-babel", function () {
    return gulp.src("src/js/*.js")
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(concat("main.js"))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("./public/js/"));
});

gulp.task('clean',
    del.bind(null, ['./.tmp'], {dot: true})
);

gulp.task('serve', function () {
  browserSync.init({
    notify: false,
    logPrefix: 'WSK',
    logFileChanges: false,
    server: ['./.tmp'],
    startPath: '/html/',
    logSnippet: false
  });
});

gulp.task('js', function () {
    return gulp.src('./src/js/**.js')
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./.tmp/js'))
})

gulp.task('sass', function () {
  return gulp.src('./src/sass/main.scss')
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
        browsers: ['last 10 versions'],
        cascade: 1
      }))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('./.tmp/css'))
      .pipe(browserSync.stream());
});

gulp.task('html', function () {
  const data = {
        j_title: ''
      },
      options = {
        ignorePartials: true,
        batch: [
          './src/html/layouts',
          './src/html/partials'
        ],
        helpers: {
          times: function (n, block) {
            let accum = '';
            for (let i = 0; i < n; ++i)
              accum += block.fn(i + 1);
            return accum;
          },
          ifCond: function (v1, v2, options) {
            if (v1 === v2) {
              return options.fn(this);
            }
            return options.inverse(this);
          }
        }
      };

  return gulp.src([
    './src/html/**/*.hbs',
    '!./src/html/layouts/**/*.hbs',
    '!./src/html/partials/**/*.hbs'
  ])
      .pipe(plumber())
      .pipe(compileHandlebars(data, options))
      .pipe(rename(path => {
        path.extname = ".html"
      }))
      .pipe(trim())
      .pipe(gulp.dest('./.tmp/html'))
      .pipe(browserSync.stream());
});

gulp.task('public', function () {
  return gulp.src('./.tmp/**/*').pipe(gulp.dest('./public'));
});

gulp.task('watch', function () {
  gulp.watch(['./src/sass/main.scss'], gulp.series('sass'));
  gulp.watch(['./src/html/**/*'], gulp.series('html'));
});

gulp.task('dev', gulp.series(
    gulp.parallel('clean'),
    gulp.parallel('js'),
    gulp.parallel('gulp-babel'),
    gulp.parallel('sass', 'html'),
    gulp.parallel('watch', 'serve')
));

gulp.task('build', gulp.series(
    gulp.parallel('clean'),
    gulp.parallel('gulp-babel'),
    gulp.parallel('sass', 'html'),
    gulp.parallel('public')
));